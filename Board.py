import pygame
from Color import Color
from Snake import Snake
from Food import Food
from Key import Key


class SnakeGame(object):
    def __init__(self, height, width):
        pygame.init()
        pygame.display.set_caption('SnakeySnakeSnake')
        self.display = pygame.display.set_mode((width, height))
        self.clock = pygame.time.Clock()
        self.blockSize = 10
        self.speed = 20
        self.width = width
        self.height = height

    def run(self):
        gameClose = False
        gameOver = False

        changePositions = (0, 0)

        snake: Snake = Snake(self.width / 2, self.height / 2)
        food: Food = Food(self.width, self.height)

        while not gameClose:
            while gameOver:
                self.display.fill(Color.SEAGREEN.value)
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == Key.QUIT.value:
                        gameClose = True
                        gameOver = False
                    if event.type == Key.PRESSED.value:
                        if event.key == Key.EXIT.value:
                            gameClose = True
                            gameOver = False
                        elif event.key == Key.RETURN.value:
                            self.run()

            for event in pygame.event.get():
                # Key.QUIT.value is pressing the 'x' button of the application
                if event.type == Key.QUIT.value:
                    gameClose = True
                if event.type == Key.PRESSED.value:
                    if event.key == Key.LEFT.value:
                        changePositions = (-self.blockSize, 0)
                    elif event.key == Key.RIGHT.value:
                        changePositions = (self.blockSize, 0)
                    elif event.key == Key.UP.value:
                        changePositions = (0, -self.blockSize)
                    elif event.key == Key.DOWN.value:
                        changePositions = (0, self.blockSize)

            (x, y) = snake.getHeadCoordinate()
            (foodX, foodY) = food.getCoordinate()
            self.display.fill(Color.TANPINKYELLOW.value)
            pygame.draw.rect(self.display, Color.OLIVEDRAB.value, [foodX, foodY, self.blockSize, self.blockSize])

            snake.moveSnake(changePositions)
            gameOver = self.isSnakeOutOfBound(x, y) or snake.isInvalidSnakePosition()
            self.drawSnake(snake)
            pygame.display.update()
            self.clock.tick(self.speed)
            # create a new Food once snake eats it
            if snake.eatBlock(food):
                food = Food(self.width, self.height)

        pygame.quit()

    def drawSnake(self, snake: Snake):
        for x in snake.body:
            pygame.draw.rect(self.display, Color.BLACK.value, [x[0], x[1], self.blockSize, self.blockSize])

    def isSnakeOutOfBound(self, x: float, y: float) -> bool:
        isBadPosition = False
        if x >= self.width or x < 0 or y >= self.height or y < 0:
            isBadPosition = True
        return isBadPosition
