from enum import Enum


class Color(Enum):
    WHITE = (255, 255, 255)
    TANPINKYELLOW = (255, 232, 205)
    BLACK = (0, 0, 0)
    OLIVEDRAB = (107, 142, 35)
    SEAGREEN = (32, 178, 170)
