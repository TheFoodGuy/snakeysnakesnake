import unittest
import mock
import Food

class FoodTest(unittest.TestCase):

    @mock.patch('Food.randrange', return_value=100)
    def test_calculatePosition(self, mockRandRange):
        result = Food.calculatePosition(200)
        self.assertEqual(result, 100)

    def test_FoodInitialization(self):
        with mock.patch.object(Food, 'calculatePosition') as mockFunc:
            food: Food = Food.Food(200, 100)
            passedArguments = mockFunc.call_args_list
            self.assertEqual(passedArguments[0].args[0], 200, "first invocation to calculatePosition")
            self.assertEqual(passedArguments[1].args[0], 100, "second invocation to calculatePosition")

    def test_FoodInitialization_calculationPositionCall(self):
        with mock.patch.object(Food, 'calculatePosition', return_value=100) as mockFunc:
            food: Food = Food.Food(200, 100)
            self.assertEqual(mockFunc.call_count, 2, "calls calculatePositions twice when initializing Food")
