from typing import List
from Food import Food


class Snake:
    def __init__(self, x: int, y: int):
        self.body: List[(int, int)] = [[x, y]]
        self.length: int = 1  # default to 1 block length
        self.headPositionX: float = x
        self.headPositionY: float = y

    def getHeadCoordinate(self) -> (float, float):
        return self.headPositionX, self.headPositionY

    def moveSnake(self, position: (float, float)) -> None:
        (x, y) = position
        self.headPositionY += y
        self.headPositionX += x
        self.body.append([self.headPositionX, self.headPositionY])
        if len(self.body) > self.length:
            del self.body[0]

    def eatBlock(self, food: Food) -> bool:
        isFoodEaten = False
        (x, y) = food.getCoordinate()
        if x == self.headPositionX and y == self.headPositionY:
            self.length += 1
            isFoodEaten = True
        return isFoodEaten

    def isInvalidSnakePosition(self) -> bool:
        for bodyPart in self.body[:-1]:
            print(bodyPart, [self.headPositionX, self.headPositionY])
            if bodyPart == [self.headPositionX, self.headPositionY]:
                return True
        return False
