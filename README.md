## SnakeySnakeSnake game

### Set up
1. Install Python3 <b>(3.8)</b>
2. Install Pygame
   1. <code>pip install pygame</code>
   
### Alternative set up
1. Install Pycharm
2. Select Pycharm 3.8 when it asks for the sdk or what python you want to use
3. Hover over pygame library in any file and it should ask you if you want to install pygame
4. go over to main.py and click on the play button next to the line number for main function
    
### Running it
1. python3 main.py
2. A window should pop up and just move the arrow keys to continue the "snake"