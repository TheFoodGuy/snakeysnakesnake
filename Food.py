from random import randrange


class Food:
    def __init__(self, boardWidth: int, boardHeight: int):
        self.positionX: float = calculatePosition(boardWidth)
        self.positionY: float = calculatePosition(boardHeight)

    def getCoordinate(self) -> (float, float):
        return self.positionX, self.positionY


def calculatePosition(position: int) -> float:
    return round(randrange(0, position) / 10.0) * 10.0
