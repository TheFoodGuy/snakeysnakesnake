import unittest
import mock

import Food
import Snake


class SnakeTest(unittest.TestCase):
    # classmethod is used for setUp Class that sets up a class once before all tests used under setUpClass
    # @classmethod
    # setUp/tearDown(self) is called prior to all tests
    def setUp(self) -> None:
        self.snake: Snake = Snake.Snake(800, 600)
        self.food: Food = Food.Food(800, 600)

    # @classmethod
    def tearDown(self) -> None:
        self.snake = None
        self.food = None

    def test_getHeadCoordinate(self):
        self.assertEqual(self.snake.getHeadCoordinate(), (800, 600), "checks to see if expected x = 800 and y = 600 "
                                                                     "are passed to the object correctly")
    def test_moveSnake_horizontally(self):
        self.snake.moveSnake((-10, 0))
        self.assertEqual(self.snake.getHeadCoordinate(), (790, 600), "checks if snake has moved to left")

    def test_moveSnake_vertically(self):
        self.snake.moveSnake((0, -10))
        self.assertEqual(self.snake.getHeadCoordinate(), (800, 590), "checks if snake has moved up")

    def test_moveSnake_RemovesItsOldHeadBlock(self):
        oldHeadPosition = self.snake.body[0]
        bodySize = len(self.snake.body)
        self.snake.moveSnake((0, 10))
        self.assertNotEqual(oldHeadPosition, self.snake.body[0], "deletes the old head block position [800, 610]")
        self.assertEqual(bodySize, len(self.snake.body), "checks to see if current body size matches after moving")

    # def test_moveSnake_AppendsNewHead(self):
    #     with mock.patch.object(Snake, "append") as mockAppend:
    #         self.snake.moveSnake((10, 0))
    #         self.assertEqual(mockAppend.call_args_list[0].args, [810, 600])

    # how to mock a method in a class instance without mocking the entire class
    # mocking getCoordinate so snake and food positions are equal and snake grows
    @mock.patch('Food.Food.getCoordinate')
    def test_eatBlock_CoordinatesDoesMatch(self, mockGetCoordinate):
        mockGetCoordinate.return_value = (800, 600)
        self.assertTrue(self.snake.eatBlock(self.food), "returns true as food is coordinates matched and food is eaten")
        self.assertEqual(self.snake.length, 2, "should grow by 1")

    @mock.patch('Food.Food.getCoordinate')
    def test_eatBlock_CoordinatesDoesNotMatch(self, mockGetCoordinate):
        mockGetCoordinate.return_value = (800, 610)
        self.assertFalse(self.snake.eatBlock(self.food), "returns false as snake y-coordinate is off by 10 to food y-coordinate")
        self.assertEqual(self.snake.length, 1, "should not grow by 1")

    def test_validateSnakePosition_HeadOnly(self):
        self.assertFalse(self.snake.isInvalidSnakePosition(), "returns false as it is only its head so position is valid")

    # by the time the snake moves to get 3 body parts, its head should not be a part of body anymore
    # only when it is initalized
    def test_validateSnakePosition_WithMultiplyParts(self):
        self.snake.length = 3
        self.snake.body += [[800, 590], [800, 580], [800, 570]]
        self.snake.moveSnake((0, 10))
        self.assertFalse(self.snake.isInvalidSnakePosition(), "returns false as head does not collide with body")

    def test_validateSnakePosition_WithMultiplyPartsCollision(self):
        self.snake.length = 3
        self.snake.body += [[800, 590], [800, 580], [800, 570]]
        self.snake.moveSnake((0, -10)) # colliding with its "neck"
        self.assertTrue(self.snake.isInvalidSnakePosition(), "returns true as head does collide with body")