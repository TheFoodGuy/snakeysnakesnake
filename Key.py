from enum import Enum

import pygame


class Key(Enum):
    PRESSED = pygame.KEYDOWN
    EXIT = pygame.K_q
    QUIT = pygame.QUIT
    RETURN = pygame.K_RETURN
    LEFT = pygame.K_LEFT
    RIGHT = pygame.K_RIGHT
    UP = pygame.K_UP
    DOWN = pygame.K_DOWN
